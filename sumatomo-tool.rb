#!/usr/bin/ruby
#
# sumatomo-tool ver 0.5.1
#
# Copyright (c) 2013 Naoki Saito
#
# This software is released under the MIT License.
#
# http://opensource.org/licenses/mit-license.php

require 'optparse'
require 'fileutils'
require 'moji'

# 管理ファイルの内容をすべて読み込んで標準出力及びCSVファイル(ファイル名 list.csv)にリストを出力する
def list_records(fileName)
	printf "管理ファイル(%s)の情報を標準出力及びリストファイル(list.csv)に出力します．\n", fileName

	if File.exist?("list.csv") 
		printf "list.csv が存在するため，コピーをバックアップします．\n"
		FileUtils.copy("list.csv", "list.csv." + Time.now.strftime("%Y%m%d_%H%M%S") + ".bak")
	end

	File.open(fileName, "rb") {|f|
		File.open("list.csv", "w") {|of|
			while record = f.read(320)
				break if record.length != 320
				begin
					fn = record[0, 255].force_encoding('SJIS').rstrip!
				rescue => ex
					printf "警告：ファイル名の処理中にエラーが発生しました．空文字として処理します．\n"
					printf "exception: %s\n", ex.message
					fn = ""
				end

				begin
					sn = record[256, 26].force_encoding('SJIS').rstrip!
				rescue => ex
					printf "警告：曲名の処理中にエラーが発生しました．空文字として処理します．\n"
					printf "exception: %s\n", ex.message
					sn = ""
				end

				begin
					an = record[282, 26].force_encoding('SJIS').rstrip!
				rescue => ex
					printf "警告：アーティスト名の処理中にエラーが発生しました．空文字として処理します．\n"
					printf "exception: %s\n", ex.message
					an = ""
				end
				
				printf "%s,%s,%s\n" , 
					fn.encode("UTF-8", :invalid => :replace, :replace => "") ,
					sn.encode("UTF-8", :invalid => :replace, :replace => "") ,
					an.encode("UTF-8", :invalid => :replace, :replace => "")

				# ファイルへは Shift-JIS + CRLF で保存
				# UTF-8で保存すると逆の変換で躓くため
				of.write(fn + "," + sn + "," + an + "\r\n")
			end
		}
	}
end

# mplist ファイルへ与えられた名前(曲名 or アーティスト名)を書き込む
# どちらも同様の仕様なので，まとめている
# データはサイズ24バイト(全角12文字)※，文字が少ない場合は残りの領域をゼロで埋める
# ※ 本当は26バイト確保されているのだが，残り2バイトの使い方は知らないので触らないことにしているだけ
# 前提：ファイルポインタがデータの先頭をポイントしていること
def write_field(outf, nameData)
	# 半角文字が含まれていたら，全角に変換
	nameZen = Moji.han_to_zen(nameData)

	# バイト列として扱いたいのでエンコーディングを変更する
	nameZen.force_encoding("ASCII-8BIT")

	# あとでゼロで埋めるバイト数
	paddingSize = 0
	
	# 24バイト以上ある場合は，24文字に切り詰める
	if nameZen.length > 24
		name = nameZen[0, 24]
		paddingSize = 0
		printf "注意：名前のサイズが12文字(24バイト)を超えるため，切り詰められます．\n"
	else
		name = nameZen
		paddingSize = 24 - name.length
	end

	# 書き込み
	outf.write(name)
	
	# 残りを0で埋める
	if paddingSize > 0 then
		zeroArr = Array.new(paddingSize, 0x00)
		outf.write(zeroArr.pack("C*"))
	end
end

# リストファイルを元に管理ファイルへ曲名，アーティスト名を出力する
# -w オプションの引数としてリストファイルを指定する．
# 曲名およびアーティスト名は12文字まで．それ以上は切り落とす．
# 前提：
#    ・リストファイルはSHIFT-JISエンコーディングを前提とする．
#    ・音楽ファイル名(*.mp3)にカンマを含まない
#    ・管理ファイル(mplist)に登録されている曲数および曲順がリストファイルと同じである
def write_records(outFile, inFile)
	printf "曲名リストファイル %s にもとづき管理ファイル(%s)の曲名とアーティスト名を更新します．\n", inFile, outFile

	# 管理ファイルのバックアップを取る
	printf "管理ファイル(%s)のコピーをバックアップします．\n", outFile
	FileUtils.copy(outFile, outFile + Time.now.strftime("%Y%m%d_%H%M%S") + ".bak")

	File.open(outFile, "r+b") {|outf|
		File.open(inFile, "r") {|inf|
			inf.each {|line|
				# 行を分割
				songData = line.force_encoding('SJIS').split(/,/, 0)
				
				# 同じファイル名が管理ファイル名に登録されているか探す
				isFound = false
				while record = outf.read(320)
					break if record.length != 320
				
					fn = record[0, 255].force_encoding('SJIS')
					if fn.rstrip == songData[0]
						printf  "ファイル名「%s」が管理ファイル(%s)中に見つかりました．\n", songData[0].encode("UTF-8"), outFile
						isFound = true
						
						# 曲名の先頭位置へ移動（レコードの先頭からオフセット0x100,次レコードからだと-0x40）
						outf.seek(-64, IO::SEEK_CUR)

						# 曲名を書き換える
						write_field(outf, songData[1])
						printf "曲名：%s を書き込みます．\n", songData[1].encode("UTF-8")

						# アーティスト名の先頭位置へ移動
						outf.seek(2, IO::SEEK_CUR)

						# アーティスト名を書き換える
						write_field(outf, songData[2].rstrip!)
						printf "アーティスト名：%s を書き込みます．\n", songData[2].encode("UTF-8")

						# 次のファイルデータの先頭に移動
						outf.seek(14, IO::SEEK_CUR)

						break
					end
				end
				
				if isFound == false
					printf  "曲名「%s」が mplist 中に見つかりませんでした．\n", songData[0].encode("UTF-8")
				end
			}
		}
	}
end


### ここから

# スマともがMP3ファイルの管理に使用するファイル名(バイナリファイル)
$mplistFile
# 更新する曲名やアーティスト名が記述されたファイル名(CSVファイル,SHIFT-JISエンコード)
$listFile

if ARGV.length < 1
	printf "usage:\n\tsumatomo-tool <mplistfile>\n\tsumatomo-tool <mplistfile> -w <listfile>\n"
	exit
end

opts = OptionParser.new
opts.on("-w listFile") {|v| $listFile = v}
opts.parse!(ARGV)

$mplistFile = ARGV[0]

if $listFile
	write_records($mplistFile, $listFile)
else
	list_records($mplistFile)
end

printf "処理が完了しました．\n"
